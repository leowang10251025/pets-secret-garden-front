import { createApp } from 'vue'
import home from './homePage.vue'
import './assets/css/style.css'

/**以下套件試引入, 未必首頁會用到 */
import '../node_modules/jquery/dist/jquery'
import '../node_modules/aos/dist/aos.css'
import '../node_modules/owl.carousel/dist/assets/owl.carousel.css'
import 'owl.carousel'

// import '../node_modules/jquery.stellar/jquery.stellar'
const stellar = require('../node_modules/jquery.stellar/jquery.stellar.js')
// import { stellar } from 'jquery.stellar'
console.log('wang', stellar)

import '../node_modules/aos/dist/aos'

// import './assets/css/open-iconic-bootstrap.min.css'
// import './assets/css/animate.css'
// // import './assets/css/owl.carousel.min.css' //裡面有圖片找不到
// import './assets/css/owl.theme.default.min.css'
// import './assets/css/magnific-popup.css'
// import './assets/css/aos.css'
// import './assets/css/ionicons.min.css'

// import './assets/css/bootstrap-datepicker.css'
// import './assets/css/jquery.timepicker.css'
// import './assets/css/flaticon.css'
// import './assets/css/icomoon.css'
// import './assets/css/style.css'

// import $ from 'jquery'
// window.$ = window.jQuery = require('jquery')

// import '@/assets/js/test.js'
// const jq = require('jquery') //https://blog.csdn.net/weekdawn/article/details/120201845
// const jq = require('vue-jquery') //https://www.npmjs.com/package/vue-jquery (待確認)
// const aos = require('aos') //https://www.npmjs.com/package/aos //OK
// const popper = require('popper.js') //https://www.npmjs.com/package/vue-popperjs
// const migrate = require('jquery-migrate') //https://www.npmjs.com/package/jquery-migrate
// const bootstrap = require('bootstrap') //https://getbootstrap.com/docs/5.0/getting-started/download/
// const easing = require('jquery.easing')(jq) //https://www.npmjs.com/package/jquery.easing
// const waypoints = require('jquery-waypoints') //https://www.npmjs.com/package/jquery-waypoints

// const stellar = require('jquery.stellar') //https://www.npmjs.com/package/jquery.stellar (引用可以，但main.js有些問題需改寫)
// const owl = require('owl.carousel') //https://www.npmjs.com/package/owl.carousel //OK
// const popUp = require('magnific-popup') //https://www.npmjs.com/package/magnific-popup
// const animate = require('jquery.animate-number') //https://www.npmjs.com/package/jquery.animate-number

// const scroll = require('vue3-smooth-scroll') //https://www.npmjs.com/package/vue3-smooth-scroll
// const datetimepicker = require('vue-bootstrap-datetimepicker') //https://www.npmjs.com/package/vue-bootstrap-datetimepicker (未安裝成功)
// const timepicker = require('jquery-timepicker') //https://www.npmjs.com/package/jquery-timepicker

// window.$ = jq
// window.$ = aos
// window.$ = popper
// window.$ = migrate
// window.$ = bootstrap
// window.$ = easing
// window.$ = waypoints
// window.$ = stellar
// window.$ = owl
// window.$ = popUp
// window.$ = animate

// aos.init() //初始化

const vm = createApp(home)
// vm.component('jqStellar', stellar)
vm.mount('#app')
